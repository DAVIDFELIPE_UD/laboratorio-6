class Tablero 
{ 
  tableroInicial()  
  {
    //variables
    let filasYcolumnas = 64;
    let i;
    //llamar el id de html
    let divTablero = document.getElementById("divTablero");
    //creacion de las matriz 8x8
      for (i = 1; i <= filasYcolumnas; i++) 
      {
        this.unaCasilla = document.createElement("input");
        this.unaCasilla.type = "button";
        this.unaCasilla.value = " ";
        this.unaCasilla.setAttribute("id", "casilla" + i);
        this.unaCasilla.setAttribute("class", "casilla");
        this.unaCasilla.setAttribute("onClick","miTablero.Ocultar(this.id)");//se llama la función ocultar, con la acción de onclick
      
        divTablero.appendChild(this.unaCasilla);
        if (i % 8 === 0) 
        {
          let salto = document.createElement("br"); //creación de un salto de linea
          divTablero.appendChild(salto);
        }
        if (this.unaCasilla.value==1)
        {
          this.unaCasilla.setAttribute("ShowHidenElement");//no entiendo para que es esto ???
        }
      }
  }
  //funcion para ocultar la casilla 
   Ocultar (id)
   {
    let miCasilla;
    miCasilla= document.getElementById(id);
    miCasilla.setAttribute("style", "visibility: hidden"); //propiedad que nos ayuda a eliminar la casilla 
   }  
}
//objeto
let miTablero = new Tablero();

